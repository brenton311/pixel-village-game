#ifndef WORLD_HPP
#define WORLD_HPP

#include "TileMap.hpp"
#include "Location.hpp"
#include "Entity.hpp"
#include <vector>

// Singleton class
class World
{
public:
    static World& getInstance();
    const Tile& getTileAt(int x, int y);
    Entity* getEntity(unsigned int id);
    void updateEntitys();
    void spawnEntity(EntityType type, Location loc);
    void render(sf::RenderWindow &target);
    ~World();
private:
    /*
        Are not to be public, they are private so
        that the class can only be used as a singleton
    */
    World();
    World(World const&);
    void operator=(World const&);

    std::vector<Entity*> m_entitys;
    TileMap m_tileMap;
    //std::vector<TileMap> tileMaps;
};

#endif // WORLD_HPP
