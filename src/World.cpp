#include <iostream>
#include "World.hpp"
#include "Player.hpp"
#include "Villager.hpp"

World::~World()
{
    for(unsigned int i = 0; i < m_entitys.size(); i++)
    {
        delete m_entitys[i];
    }
}


World::World()
{
    std::cout << "Loading tilemap!" << std::endl;
    if(!m_tileMap.loadFromFile("assets/world/level.txt"))
    {
        // Error
    }
}


void World::updateEntitys()
{
    for(unsigned int i = 0; i < m_entitys.size(); i++)
    {
        m_entitys[i]->update();
    }
}

World& World::getInstance()
{
    static World instance;
    return instance;
}

void World::spawnEntity(EntityType type, Location loc)
{
    unsigned int newID = 0;

    // Don't try to get an entity if zero have been spawned
    if(m_entitys.size() != 0)
    {
        newID = m_entitys[ m_entitys.size()] -> getID() + 1;
    }

    std::cout << "Spawning new Entity with ID: " << newID << "!" << std::endl;

    switch(type)
    {
    case EntityType::PLAYER:
        {
            Player* player = new Player(Location( loc.getX(), loc.getY()), newID);
            m_entitys.push_back(player);
            break;
        }
    case EntityType::VILLAGER:
        {
            Villager* vill = new Villager(Location( loc.getX(), loc.getY()), newID);
            m_entitys.push_back(vill);
            break;
        }
    default:
        std::cout << "Error in entity spawning: Invalid ID!" << std::endl;
        //m_entitys.push_back(new Entity(Location( loc.getX(), loc.getY(), type, newID));
        break;
    }
}


Entity* World::getEntity(unsigned int id)
{
    std::vector<Entity>::iterator eIterator;

    for(unsigned int i = 0; i < m_entitys.size(); i++)
    {
        if(m_entitys[i]->getID() == id)
            return m_entitys[i];
    }
    return nullptr;
}

void World::render(sf::RenderWindow &target)
{
    static sf::Sprite tile;
    static sf::Texture texture;
    static bool hasRunBefore = false;

    if(!hasRunBefore)
    {
        std::cout << "Setting up world!" << std::endl;
        texture.loadFromFile("assets/terrain/stone.png");
        texture.setRepeated(true);
//        rect.setSize(sf::Vector2f(16, 16));
        //rect.setOrigin(0, 0);
        tile.setTexture(texture);
        hasRunBefore = true;
    }


    for(int x = 0; x < 16; x++)
    {
        for(int y = 0; y < 16; y++)
        {
            //std::cout << x << " " << y << std::endl;
            //rect.setOrigin(x * 16, y * 16);
            tile.setPosition(x * 16, y * 16);
            //rect.setOrigin(rect.getOrigin().x + x * 16, rect.getOrigin().x + y * 16);
            target.draw(tile);
        }
    }
}
