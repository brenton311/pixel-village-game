#ifndef LOCATION_HPP
#define LOCATION_HPP

#include <SFML/System.hpp>

/*
    Useful class for describing location of entity's
    or tiles
*/

class Location
{
    public:
        Location(float x, float y);
        float getX();
        float getY();
        void setX(float x);
        void setY(float y);
        int getTileX();
        int getTileY();
    private:
        sf::Vector2f m_pos;
};

#endif // LOCATION_HPP
