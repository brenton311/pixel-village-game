#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "RenderableEntity.hpp"
#include "Location.hpp"


/*
    A base class for all entitys living (such as players)
    or non-living (boats - to be added later)
*/
class Entity : public RenderableEntity
{
public:
    Entity(Location loc, EntityType type, unsigned int id);
    virtual ~Entity();
    virtual void move(Location loc);
    virtual void update();          // Update AI or anything else that needs to be updated
    virtual unsigned int getID();
    virtual Location getLocation();
protected:
    const EntityType m_entityType;
    const unsigned int m_id;
    unsigned int m_health;
    //const int m_intMaxHealth;
    float m_velocity;           // The velocity in pixels per 40th of a second
    Location m_location;
};

#endif // ENTITY_HPP
