#include <iostream>
#include <SFML/System.hpp>
#include "Player.hpp"
#include "Location.hpp"

//#include <iostream>

Player::Player(Location loc, unsigned int id)
    : Entity(loc, EntityType::PLAYER, id)
{
    m_state = EntityState::RESTING;

    // Load the character's sprite sheet and only setup textures if it loads correctly
/*    if(!setupTextures())
    {
        //std::cout << "Error in loading character sprite sheet!" << std::endl;
    }*/
}

Player::~Player()
{

}


// Update the players position based on the keyboard input
void Player::update()
{
    Location oldLoc(m_location.getX(), m_location.getY());

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        this->move( Location(m_location.getX() - m_velocity, m_location.getY() ));
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        this->move(Location( m_location.getX() + m_velocity, m_location.getY() ));
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        this->move(Location( m_location.getX(), m_location.getY() - m_velocity ));
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        this->move(Location( m_location.getX(), m_location.getY() + m_velocity ));
    }

    if(oldLoc.getX() == m_location.getX() && oldLoc.getY() == m_location.getY())
    {
        m_movementState = EntityState::RESTING;
        m_sprite.setTextureRect(m_resting);
    }
}

void Player::move(Location loc)
{
    std::cout << "Player Move!" << std::endl;
    /* If the player is in the same position set
        them to a resting position, this is unique
        to the player and not over entitys
    */
    if(loc.getX() == m_location.getX() && loc.getY() == m_location.getY())
    {
        this->m_state = EntityState::RESTING;
        m_sprite.setTextureRect(m_resting);
        return;
    }

    // Continue movement like any other entity
    Entity::move(loc);
}
