#ifndef TILE_HPP
#define TILE_HPP

#include "BlockUtl.hpp"

class Tile
{
public:
    Tile(TileID id);
    virtual ~Tile();
protected:
    TileID m_id;
};

#endif // TILE_HPP
