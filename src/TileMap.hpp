#ifndef TILEMAP_HPP
#define TILEMAP_HPP

#include <vector>
#include <string>
#include "Tile.hpp"

class TileMap
{
public:
    TileMap();
    virtual ~TileMap();
    bool loadFromFile(const std::string& path);
private:
    /*
        Contains all of the tiles loaded in a 16x16 area,
        where each tile is (16 pixels X 16 pixels),
        so you can access it like: tiles[xCoord][yCoord]
    */
    TileID tiles[16][16];
};

#endif // TILEMAP_HPP
