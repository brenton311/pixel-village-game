#ifndef VILLAGER_HPP
#define VILLAGER_HPP

#include "Entity.hpp"

class Villager : public Entity
{
public:
    Villager(Location loc, unsigned int id);
    virtual ~Villager();
    virtual void update();
};

#endif // VILLAGER_HPP
