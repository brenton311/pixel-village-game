#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H

/*
    Singleton class for easy management of textures.
    Reduces the amount of texture loading needed
*/

#include <map>
#include <SFML/Graphics.hpp>

class TextureManager
{
public:
    static TextureManager& getInstance();
    sf::Texture& getTexture(std::string path);
    ~TextureManager(){}
private:
    std::map<std::string, sf::Texture> m_loadedTextures;
    TextureManager();
    TextureManager(TextureManager const&);
    void operator=(TextureManager const&);
//    static std::map<std::string, sf::Texture> s_loadedTextures;
};

#endif // TEXTUREMANAGER_H
