#ifndef WINDOWMANAGER_HPP
#define WINDOWMANAGER_HPP

#include <SFML/Graphics.hpp>

namespace WindowManager
{
bool isActive();
void setActive(bool flag);
void create();
void handleEvents();
sf::RenderWindow getWindow();
}

//class WindowManager
//{
//    public:
//        WindowManager &getInstance();
//        void create();
//        void setActive(bool flag);
//        bool isActive();
//    private:
//        static bool s_active;
//        static sf::RenderWindow s_window;
//};

#endif // WINDOWMANAGER_HPP
