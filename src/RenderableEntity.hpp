#ifndef RENDERABLEENTITY_HPP
#define RENDERABLEENTITY_HPP

#include <SFML/Graphics.hpp>
#include <string>
#include "Location.hpp"

enum EntityType
{
    PLAYER,
    VILLAGER
};

enum EntityState
{
    RESTING,
    MOVING_FORWARD,
    MOVING_RIGHT,
    MOVING_LEFT,
    MOVING_REVERSE
};

/*
    A base class that handles all of the animation
    for an entity.
*/
class RenderableEntity
{
public:
    RenderableEntity(Location loc, EntityType type);
    virtual ~RenderableEntity();
    virtual const sf::Sprite& getSprite();
private:
    void setEntityTexturePath();
protected:
    bool setupTextures();
    virtual void updateAnimation();

    // Contains all of the locations of the character sprites
    sf::IntRect m_movingForward[3];
    sf::IntRect m_movingReverse[3];
    sf::IntRect m_movingRight[3];
    sf::IntRect m_movingLeft[3];
    sf::IntRect m_resting;

    EntityType m_EntityType;
    sf::Texture m_spriteSheet;
    unsigned int m_movementState;
    EntityState m_state;
    sf::Sprite m_sprite;
    std::string m_texturePath;

};

#endif // RENDERABLEENTITY_HPP
