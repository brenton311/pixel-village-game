#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <SFML/Graphics.hpp>
#include "Entity.hpp"

class Player : public Entity
{
public:
    Player(Location loc, unsigned int id);
    virtual ~Player();
    virtual void update();
    void move(Location loc);
private:

};

#endif // PLAYER_HPP
