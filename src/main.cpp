#include <iostream>
#include <SFML/Window.hpp>
#include <sstream>

#include "WindowManager.hpp"
#include "Player.hpp"
#include "Villager.hpp"
#include "World.hpp"

const float velocity = 3;
bool active = true;
//Villager vill(100, 100);

void handleInput(sf::RenderWindow &window);
void handleWindowEvents(sf::RenderWindow &window);
std::string intToString(const int &x);

int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "BShooter!");
    //window.setMouseCursorVisible(false);
    window.setVerticalSyncEnabled(false);
    window.setFramerateLimit(150);



    // Load the icon/image for the window
    sf::Image icon;
    if(!icon.loadFromFile("assets/icon.bmp"))
    {
        std::cout << "Failed to load icon!" << std::endl;
        return -1;
    }
    window.setIcon(32, 32, icon.getPixelsPtr());

//    Player player(window.getSize().x / 2 , window.getSize().y / 2);

    sf::Clock secondCounter;

    // For FPS and loop ticks
    sf::Time timeSinceLastUpdate;
    sf::Clock tickClock;
    sf::Clock secondsClock;
//    unsigned int numFramesPerSecond = 0;
    int numOfUpdates = 0;


    World::getInstance();
    World::getInstance().spawnEntity(EntityType::PLAYER, Location(window.getSize().x / 2, window.getSize().y / 2));


    // 40 TPS = Desired TPS
    while(window.isOpen())
    {
        handleWindowEvents(window);


        /*  if(secondsClock.getElapsedTime() >= sf::seconds(1))
          {
              std::cout << "# of updates:"  << std::endl;
              std::cout << "\t" << numOfUpdates << "!" << std::endl;
              secondsClock.restart();
              numOfUpdates = 0;
          }   */

        // Update only if 25 milliseconds have passed (40 TPS)
        if(tickClock.getElapsedTime() > sf::milliseconds(25))
        {
            // Update
            handleInput(window);

            // Handle the players movement by moving the windows view
            sf::View view = window.getView();
            float oldX = World::getInstance().getEntity(0)->getLocation().getX(), oldY = World::getInstance().getEntity(0)->getLocation().getY();
            World::getInstance().updateEntitys();
            view.move(World::getInstance().getEntity(0)->getLocation().getX() - oldX, World::getInstance().getEntity(0)->getLocation().getY() - oldY);
            window.setView(view);

            //World::getInstance().updateEntitys();


//            vill.update();

            // Draw
            window.clear();
            World::getInstance().render(window);
//            window.draw(vill.getSprite());
            window.draw(World::getInstance().getEntity(0)->getSprite());
            window.display();

            tickClock.restart();
            numOfUpdates++;
        }
        else
        {
            /*
                The program has extra time to sleep until the next update so
                just sleep and render corresponding with the max frame rate the user set
            */
            sf::Time timeToSleep;
            timeToSleep = (sf::milliseconds(25) - tickClock.getElapsedTime());
            sf::sleep(timeToSleep);
        }

    }

    return 0;
}


std::string intToString(const int &x)
{
    std::string result;
    std::ostringstream convert;
    convert << x;
    return convert.str();
}

void handleWindowEvents(sf::RenderWindow &window)
{
    // Handle window events
    sf::Event event;
    while(window.pollEvent(event))
    {
        // Handle player movement
        if(event.type == sf::Event::Closed)
        {
            window.close();
        }

        if(event.type == sf::Event::LostFocus)
        {
            std::cout << "Not Active!" << std::endl;
            active = false;
        }

        if(event.type == sf::Event::GainedFocus)
        {
            std::cout << "Active!" << std::endl;
            active = true;
        }

        if(event.type == sf::Event::Resized)
        {
            // update the view to the new size of the window
            std::cout << "Resizing!" << std::endl;
            sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);
            window.setView(sf::View(visibleArea));
        }
    }
}


void handleInput(sf::RenderWindow &window)
{
    // Only handle input if the window is active
    if(!active)
        return;

    // Take a screen shot
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::F2))
    {
        sf::Image image = window.capture();
        image.saveToFile("screenshot.png");
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
    {
        window.close();
        return;
    }
}
