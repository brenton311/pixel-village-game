#include <iostream>
#include "Entity.hpp"
#include "Player.hpp"
#include "Location.hpp"


Entity::Entity(Location loc, EntityType type, unsigned int id)
    : RenderableEntity(loc, type), m_entityType(type), m_id(id), m_health(20),
        m_velocity(3), m_location(loc)
{
}

Entity::~Entity()
{
    //dtor
}

unsigned int Entity::getID()
{
    return m_id;
}


void Entity::update()
{
    std::cout << "Invalid Default Entity Update!" << std::endl;
}

void Entity::move(Location loc)
{
    // If the loc.getX() is different the entity is moving towards the right
    if(loc.getX() > m_location.getX())
    {
        updateAnimation();
        this->m_state = EntityState::MOVING_RIGHT;
        m_sprite.setTextureRect(m_movingRight[m_movementState]);
    }
    // If the loc.getX() is less the entity is moving towards the left hand of the screen
    else if(loc.getX() < m_location.getX())
    {
        updateAnimation();
        this->m_state = EntityState::MOVING_LEFT;
        m_sprite.setTextureRect(m_movingLeft[m_movementState]);
    }
    // If the y is greater the entity is moving down on the screen
    else if(loc.getY() > m_location.getY())
    {
        updateAnimation();
        this->m_state = EntityState::MOVING_REVERSE;
        m_sprite.setTextureRect(m_movingReverse[m_movementState]);
    }
    // If the y is greater the entity is moving down on the screen
    else if(loc.getY() < m_location.getY())
    {
        updateAnimation();
        this->m_state = EntityState::MOVING_FORWARD;
        m_sprite.setTextureRect(m_movingForward[m_movementState]);
    }

    //std::cout << "New X: " << loc.getX() << std::endl;
    //std::cout << "New Y: " << loc.getY() << std::endl;
    m_location.setX(loc.getX());
    m_location.setY(loc.getY());
    m_sprite.setPosition(m_location.getX(), m_location.getY());
/*
    this->m_loc.getX()Pos = loc.getX();
    this->m_yPos = y;
    this->m_sprite.setPosition(m_loc.getX()Pos, m_yPos);*/
}


Location Entity::getLocation()
{
    return m_location;
}
