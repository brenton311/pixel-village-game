#include <iostream>
#include <assert.h>
#include "BlockUtl.hpp"
#include "TextureManager.hpp"

sf::Texture& BlockUtl::getTileTexture(TileID id)
{
    switch(id)
    {
    case 0:
        return TextureManager::getInstance().getTexture("assets/terrain/air.png");
        break;
    case 1:
        // Stone
        return TextureManager::getInstance().getTexture("assets/terrain/stone.png");
        break;
    case 2:
        // Grass
        return TextureManager::getInstance().getTexture("assets/terrain/grass.png");
        break;
    case 3:
        // Water
        return TextureManager::getInstance().getTexture("assets/terrain/water.png");
        break;
    case 4:
        // Sand
        return TextureManager::getInstance().getTexture("assets/terrain/sand.png");
        break;
    case 5:
        // Gravel
        return TextureManager::getInstance().getTexture("assets/terrain/gravel.png");
        break;

    default:
        std::cout << "Failed to get tile texture!" << std::endl;
        break;
    }

    // If the tile id can't be found use the missing texture.
    return TextureManager::getInstance().getTexture("assets/missingtexture.png");
}


// Get the TileID from a provided int
TileID BlockUtl::parseID(int id)
{
    switch(id)
    {
    case 0:
        return TileID::AIR;
        break;
    case 1:
        return TileID::STONE;
        break;
    case 2:
        return TileID::GRASS;
        break;
    case 3:
        return TileID::WATER;
        break;
    case 4:
        return TileID::SAND;
        break;
    case 5:
        return TileID::GRAVEL;
        break;
    default:
        std::cout << "Invalid id: " << id << "!" << std::endl;
        return TileID::GRASS;
        break;
    }
}
