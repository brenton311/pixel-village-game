#include "Location.hpp"

/*
    Useful class for describing location of entity's
    or tiles
*/

Location::Location(float x, float y)
{
    m_pos.x = x;
    m_pos.y = y;
}

void Location::setX(float x)
{
    m_pos.x = x;
}

void Location::setY(float y)
{
    m_pos.y = y;
}

int Location::getTileX()
{
    return (int)m_pos.x;
}

int Location::getTileY()
{
    return (int)m_pos.y;
}

float Location::getX()
{
    return m_pos.x;
}

float Location::getY()
{
    return m_pos.y;
}
