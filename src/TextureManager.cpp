#include <iostream>
#include "TextureManager.hpp"


TextureManager::TextureManager()
{
    // TODO: load the textures that are needed throughout the game at startup
}

TextureManager& TextureManager::getInstance()
{
    static TextureManager instance;
    return instance;
}

sf::Texture& TextureManager::getTexture(std::string path)
{
    // Check to see if the texture has not been loaded before
    if(!m_loadedTextures.count(path))
    {
        sf::Texture temp;
        if(!temp.loadFromFile(path))
        {
            // Error, return the error texture
            std::cout << "Failed to load texture: " << path << std::endl;
            return TextureManager::getTexture("assets/missingtexture.png");
        }
        m_loadedTextures[path] = temp;
        return m_loadedTextures[path];
    }
    return m_loadedTextures[path];
}
