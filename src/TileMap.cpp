#include <iostream>
#include <fstream>
#include "TileMap.hpp"
#include "BlockUtl.hpp"

TileMap::TileMap()
{

    //tiles = new Tile[16][16](BlockUtl::GRASS);
}

TileMap::~TileMap()
{
    //dtor
}



bool TileMap::loadFromFile(const std::string& path)
{
    // Open the file and make sure it opened
    std::ifstream file(path);
    if(!file.good())
    {
        std::cout << "Failed to open file: " << path << "!" << std::endl;
        return false;
    }

    std::cout << "Loading World!" << std::endl;
    int temp;

    for( unsigned int x = 0; x < 16; x++ )
    {
        /*file >> temp;
        std::cout << "X: " << x << "\tValue: " << temp << std::endl;
        */

        for( unsigned int y = 0; y < 16; y++ )
        {
            file >> temp;
            //std::cout << "Y: " << y << " X:" << x << "\tValue: " << temp << std::endl;
            tiles[x][y] = BlockUtl::parseID(temp);
        }
    }

    std::cout << "Loaded World!" << std::endl;

//    while(file::)

    file.close();
    return true;
}
