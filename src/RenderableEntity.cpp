#include <iostream>
#include "TextureManager.hpp"
#include "RenderableEntity.hpp"

RenderableEntity::RenderableEntity(Location loc, EntityType type)
    :  m_EntityType(type), m_movementState(0), m_state(EntityState::RESTING)
{
    m_sprite.setPosition(loc.getX(), loc.getY());
    setupTextures();
}

RenderableEntity::~RenderableEntity()
{
    //dtor
}

/*
    Based on the entity id's set the path to the sprite sheet
*/
void RenderableEntity::setEntityTexturePath()
{
    switch(m_EntityType)
    {
    case PLAYER:
        m_texturePath = "assets/player.png";
        break;
    case VILLAGER:
        m_texturePath = "assets/villager.png";
        break;
    default:
        std::cout << "Failed to find texture for entity id: " << m_EntityType << std::endl;
        break;
    }
}


bool RenderableEntity::setupTextures()
{
    setEntityTexturePath();
    /*sf::Texture texture = TextureManager::getTexture(m_texturePath);
    std::cout << "Texture Path: " << m_texturePath << "!" << std::endl;*/

    m_sprite.setTexture(TextureManager::getInstance().getTexture(m_texturePath));


    /* The three position are the sprite for the character while they
    are moving to make it look like they are walking*/

    m_movingForward[0] = sf::IntRect(0, 96, 32, 32);
    m_movingForward[1] = sf::IntRect(32, 96, 32, 32);
    m_movingForward[2] = sf::IntRect(64, 96, 32, 32);

    m_movingReverse[0] = sf::IntRect(0, 0, 32, 32);
    m_movingReverse[1] = sf::IntRect(32, 0, 32, 32);
    m_movingReverse[2] = sf::IntRect(64, 0, 32, 32);

    m_movingRight[0] = sf::IntRect(0, 64, 32, 32);
    m_movingRight[1] = sf::IntRect(32, 64, 32, 32);
    m_movingRight[2] = sf::IntRect(64, 64, 32, 32);

    m_movingLeft[0] = sf::IntRect(0, 32, 32, 32);
    m_movingLeft[1] = sf::IntRect(32, 32, 32, 32);
    m_movingLeft[2] = sf::IntRect(64, 32, 32, 32);


    m_resting = m_movingReverse[1];                // Resting should look just like it is moving down

    m_sprite.setTextureRect(m_resting);
    return true;
}



const sf::Sprite& RenderableEntity::getSprite()
{
    return m_sprite;
}
void RenderableEntity::updateAnimation()
{
    static sf::Clock clock;

    // Only update the movement sprite every 300 millisecondss
    if(clock.getElapsedTime() < sf::milliseconds(300))
    {
        //std::cout << "Not updating!" << std::endl;
        return;
    }
    else
        clock.restart();

    if(m_movementState == 2)
    {
        m_movementState = 0;
        return;
    }
    m_movementState++;
}
