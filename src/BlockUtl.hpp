#ifndef BlockUtl_HPP
#define BlockUtl_HPP

#include <iostream>
#include <SFML/Graphics.hpp>

enum TileID
{
    AIR,
    GRASS,
    STONE,
    WATER,
    GRAVEL,
    SAND
};

class BlockUtl
{
public:
    static TileID parseID(int id);
    static sf::Texture& getTileTexture(TileID id);
};


/*
BlockUtl getBlockFromID(int id)
{

    }
}*/

#endif
